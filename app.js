var Client = require('node-rest-client').Client;
var client = new Client();
var jsonfile = require('jsonfile');
var file = 'normalize.json';

const URL = 'http://staging.foodee.io:10001';
const TOKEN = '75f8d315-4531-40d1-ad26-6fa511fed82b';

var template = {
	'restaurant': {
		'location': {
			'address': 'Nha hang 5 sao cua',
			'city': 'Kentucky',
			'country': 'US',
			'state': 'KT',
			'zip': '21030',
			'latitude': '37.534777',
			'longitude': '-84.726563'
		},
		'name': 'Khang Restaurant'
	},

	'rating': 1,
	'pricing': 2,
	'categories': [1, 2, 5],
	'collections': ['cf72bb39-140a-40ad-8b03-bdcf4711ab19']
};

jsonfile.readFile(file, function (err, json_array) {
	for (var i = 1; i < json_array.length; i++) {
		template.title = json_array[i].Name;
		template.text = json_array[i].Comment;
		template.photo = i + '.jpg';
		var args = {
			data: template,
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': TOKEN
			}
		};

		client.post(URL + '/me/posts', args, function (data, response) {
			// raw response
			console.log(data);
		});
	}
});
